package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String[] liste_salles;

    private String salle;
    private String poste;
    private String DISTANCIEL;
    // TODO Q2.c
    private SuiviViewModel model;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        liste_salles = getResources().getStringArray(R.array.list_salles);
        DISTANCIEL = liste_salles[0];
        poste = "";
        salle = DISTANCIEL;
        // TODO Q2.c
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);

        // TODO Q4
        Spinner spSalle = getActivity().findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> adapterSalle = ArrayAdapter.createFromResource(getContext(), R.array.list_salles, android.R.layout.simple_spinner_item);
        spSalle.setAdapter(adapterSalle);

        Spinner spPoste = getActivity().findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> adapterPoste = ArrayAdapter.createFromResource(getContext(), R.array.list_postes, android.R.layout.simple_spinner_item);
        spPoste.setAdapter(adapterPoste);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
            EditText tvLogin = getActivity().findViewById(R.id.tvLogin);
            model.setUsername(tvLogin.getText().toString());

        });

        // TODO Q5.b
        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        this.update();
        // TODO Q9
    }

    // TODO Q5.a
    private void update() {
        Spinner spSalle = getActivity().findViewById(R.id.spSalle);
        Spinner spPoste = getActivity().findViewById(R.id.spPoste);
        if(spSalle.getSelectedItem() == DISTANCIEL){
            spPoste.setVisibility(View.GONE);
            spPoste.setEnabled(false);
            this.model.setLocalisation("Distanciel");
        }else{
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            this.model.setLocalisation(spSalle.getSelectedItem().toString()+" : "+spPoste.getSelectedItem().toString());
        }
    }

    // TODO Q9
}